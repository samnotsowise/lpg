# Linkable Password Generator

For when you want to send someone a link to generate a password that's as secure as you allow.

100% on-device password generation, this system cannot log or rebuild the generated passwords. 

[Try It Now](https://samnotsowise.gitlab.io/lpg/)
