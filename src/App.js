import React, { useCallback, useState, useEffect } from 'react';
import { Header, Segment, Grid, Form, Button, Input } from 'semantic-ui-react';
import { useUrlParams } from './useUrlParams';

const yes = 'y';
const no = 'n';

const lcSrc = 'abcdefghijklmnopqrstuvwxyz';
const ucSrc = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
const numSrc = '0123456789';
const symSrc = '!@£$%^&*()_+-=[]{};\'\\:"|,./<>?`~#"';

export const App = () => {
  const { l = 120, lc = yes, uc = yes, num = yes, sym = yes, setParam } = useUrlParams();
  const set = (param) => (_, { checked }) => setParam(param, checked ? undefined : no);

  const generate = useCallback(() => {
    const source = [
      lc === yes && lcSrc,
      uc === yes && ucSrc,
      num === yes && numSrc,
      sym === yes && symSrc,
    ].filter(Boolean).join("") || lcSrc;
    const sourceLength = source.length;
    let output = '';
    while(output.length < l) {
      output += source[Math.floor(Math.random() * sourceLength)]
    }
    return output;
  }, [lc, uc, num, sym, l]);

  useEffect(() => setGenerated(generate()), [generate]);

  const [generated, setGenerated] = useState(generate());

  return (
    <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
      <Grid.Column style={{ maxWidth: 400 }}>
        <Header as='h1'>Linkable Password Generator</Header>
        <Form size='large'>
          <Segment stacked>
            <Form.Input fluid type='number' value={l} onChange={(_, { value }) => setParam('l', value)} label="Length" />
            <Form.Checkbox fluid label="lower case" checked={lc === yes} onChange={set('lc')} />
            <Form.Checkbox fluid label="UPPER CASE" checked={uc === yes} onChange={set('uc')} />
            <Form.Checkbox fluid label="numbers" checked={num === yes} onChange={set('num')} />
            <Form.Checkbox fluid label="symbols" checked={sym === yes} onChange={set('sym')} />
            <Button color='teal' fluid size='large' onClick={() => setGenerated(generate())}>
              Generate
            </Button>
          </Segment>
          <Input
            fluid
            id="o"
            action={{
              color: 'teal',
              labelPosition: 'right',
              icon: 'copy',
              content: 'Copy',
              onClick: () => {
                const target = document.getElementById("o");
                target.select();
                target.setSelectionRange(0, target.value.length);
                document.execCommand("copy");
              },
            }}
            value={generated}
          />
        </Form>
      </Grid.Column>
    </Grid>
  );
};
