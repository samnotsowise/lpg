import { useState } from 'react';

export const useUrlParams = () => {
  const [urlState, setUrlState] = useState({});
  const urlParams = new URLSearchParams(window.location.search);
  return {
    ...Object.fromEntries(urlParams.entries()),
    ...urlState,
    setParam: (name, value) => {
      const param = name.toLowerCase();
      setUrlState({...urlState, [param]: value});
      value ? urlParams.set(param, value) : urlParams.delete(param);
      window.history.pushState(
        Object.fromEntries(urlParams.entries()),
        window.document.title,
        '?' + urlParams.toString(),
      );
    },
    getParam: (name) => urlParams.get(name.toLowerCase()),
  };
};
